package tehran.sematec.mylibrary.oop;

public class Student extends Human {

    private void init() {
        setName("Amir");
        setFamily("Teymoori");
        System.out.println(getName() + " " + getFamily());
    }

    @Override
    void setName(String name) {
        super.setName(name);
    }
}
