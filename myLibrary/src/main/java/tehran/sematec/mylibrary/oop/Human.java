package tehran.sematec.mylibrary.oop;

public class Human {

    private String name;
    protected String family;
    String city;
    public int age;

    public String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getFamily() {
        return family;
    }

    void setFamily(String family) {
        this.family = family;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
