package tehran.sematec.mylibrary.s2;

public class FriendModel {

    String name,family , job ;

    public FriendModel(String name, String family, String job) {
        this.name = name;
        this.family = family;
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
}
