package tehran.sematec.mylibrary.s2;

public class BookClass {

    private String name = "Sadegh Hedayat";
    private String isbn;

    private String family = "sadegh";
    private int count;
    private String author;

    int age = 17;
    int price = 100;
    long number = 2323242433L;
    float pNumber = 3.147f;
    double pExactNumber = 3.14333333333333333333;

    boolean isAlive = true;
    boolean isGood = false;


    private void test() {
        //compare booleans
        if (isAlive == isGood) {
            System.out.println("equals");
        } else {
            System.out.println("not equals");
        }

        //compare int
        if (age == price) {
            //return true
        } else {
            //return false
        }

        //compare strings
        // if( name.equals(family))

        String name = "Babak";
        String nickName = "babak";
        //  if( name.equals(nickName))   >> false
        //   if (name.equalsIgnoreCase(nickName))  >> true

        String concatNames = name + " " + nickName;
        //Babak babak

        //todo remove this lines


        if (isAlive == true) {

        }
        //same
        if (isAlive) {
            System.out.println("IsAlive");
            System.out.println("IsAlive");
        }
        if (isAlive)
            System.out.println("IsAlive");

        if (isAlive != true) {

        }
        if (!isAlive){

        }

        if( age!=price){

        }


    }



    private String getMyName(){

        if(age==10)
            return "Ali";
        else if(age ==20){
            return "JAvad" ;
        }
//        else{
//            return "no name" ;
//        }
//
        return "unknown" ;
    }


    //998 is my code
    //2 is my age
    public float getPNumber() {
        return 3.14f + 998 - 2;
    }


    private String country;

    public BookClass(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
