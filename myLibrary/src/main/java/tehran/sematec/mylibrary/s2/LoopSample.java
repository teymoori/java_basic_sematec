package tehran.sematec.mylibrary.s2;

public class LoopSample {


    public static void main(String[] n) {

        for (int i = 50; i >= 0; i--) {
            System.out.println(i);
        }
        printSpace() ;

        int count = 0;
        while (count < 100) {
            System.out.println(count);
            count++;
        }
        printSpace() ;

        int age = 0;
        do {

            System.out.println(age);
            age++;

        } while (age < 200);


    }


    static void printSpace() {
        System.out.println("**************");
        System.out.println("**************");
        System.out.println("**************");
    }
}
