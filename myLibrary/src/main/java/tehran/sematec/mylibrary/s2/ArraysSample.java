package tehran.sematec.mylibrary.s2;


import java.util.ArrayList;
import java.util.List;

public class ArraysSample {

    static List<String> students = new ArrayList<>();

    static List<String> mobiles = new ArrayList<>();

    static List<BookClass> books = new ArrayList<>();

    static List<HomeClass> homes = new ArrayList<>();

    public static void main(String[] args) {
        students.add("Alireza Mohammadi");
        students.add("Marayam JAvadi");
        students.add("HAsan");
        for (String name : students) {
            System.out.println(name);
        }
        mobiles.add("0912237823");


        HomeClass h1 = new HomeClass();
        h1.setAddress("Tehran Vanak");
        h1.setCity("Tehran");
        h1.setPrice(99999999);
        h1.setPelak(33);
        Owner owner = new Owner("ali", "hasani", "30", "091231312") ;
//        owner.setFriends();


        h1.setOwner(owner);




        HomeClass h2 = new HomeClass();
        h2.setAddress("Gohardasht");
        h2.setCity("KAraj");
        h2.setPrice(1231231231);
        h2.setPelak(10);
        h2.setOwner(new Owner("Mohammad", "rezayi", "33", "0936324824"));

        homes.add(h1);
        homes.add(h1);
        homes.add(h1);
        homes.add(h1);
        homes.add(h1);
        homes.add(h2);
        homes.add(h2);
        homes.add(h2);
        homes.add(h2);
        homes.add(h2);
        homes.add(h2);

        homes.add(new HomeClass("street Emam", "Kerman"));

//        for (HomeClass home : homes) {
//            System.out.println(home.getCity() + " " + home.getAddress());
//        }


        System.out.println(
                homes.get(4).getCity() + " " +
                        homes.get(4).getAddress() + " "
                        + " owner : " +
                        homes.get(4).getOwner().getName() + " " +
                        homes.get(4).getOwner().getFamily()
        );


    }


}
