package tehran.sematec.mylibrary.s2;

import java.util.List;

public class Owner {

    String name, family, age, mobile;

    List<FriendModel> friends;


    public Owner(String name, String family, String age, String mobile) {
        this.name = name;
        this.family = family;
        this.age = age;
        this.mobile = mobile;
    }

    public List<FriendModel> getFriends() {
        return friends;
    }

    public void setFriends(List<FriendModel> friends) {
        this.friends = friends;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
