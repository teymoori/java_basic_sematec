package tehran.sematec.mylibrary.s2;

public class CarHandlerClass {

    int model;
    String color, name, company, type;



    public CarHandlerClass(String inputName, String company) {
        this.name = inputName;
        this.company = company;
    }


    public CarHandlerClass(int model, String color, String name, String company) {
        this.model = model;
        this.color = color;
        this.name = name;
        this.company = company;
    }

    public int getPrice() {
        if (this.name.equals("Pride"))
            return 20000000;
        else if (this.name.equals("Samand"))
            return 30000000;
        else return 0;
    }
}
