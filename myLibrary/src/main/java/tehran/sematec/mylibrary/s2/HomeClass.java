package tehran.sematec.mylibrary.s2;


//pojo entity DTO Object
public class HomeClass {

    private String address, city;
    private int pelak, price;


    private Owner owner ;


    public HomeClass() {
    }

    public HomeClass(String address, String city) {
        this.address = address;
        this.city = city;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPelak(int i) {
        return pelak;
    }

    public void setPelak(int pelak) {
        this.pelak = pelak;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
