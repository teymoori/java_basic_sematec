package tehran.sematec.mylibrary.realtions;

public class BookDTO {
    String name,price ;

    Author author ;

    @Override
    public String toString() {
        return "BookDTO{" +
                "name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", author=" + author.toString() +
                '}';
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }


}
