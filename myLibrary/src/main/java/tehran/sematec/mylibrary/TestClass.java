package tehran.sematec.mylibrary;

public class TestClass {


    // PascalCase >>> classes
    //camelCase  >>> functions,variables


    static String myCity = "Tehran";


    public static void main(String[] n) {
        System.out.println("inside Test Class");
        System.out.println(getMyName());

//        CarHandlerClass car = new CarHandlerClass() ;
//        car.getCarName()

    }

    static String getMyName() {
        return "AmirHossein";
    }

    static int getMyAge() {
        return 30;
    }

    static void printMyName(String name) {
        System.out.println(name);
    }

    static int sin(int x) {
        if (x == 90)
            return 1;
        if (x == 0)
            return 0;

        return 1;
    }

}
